drop database if exists bookshop;

create database bookshop;

use bookshop;


create table client
(
    client_id                 int          not null primary key auto_increment,
    client_name               varchar(255) not null unique,
    client_phone              varchar(255) not null,
    client_post_index         varchar(255) not null,
    client_post_index_address varchar(255) not null,
    client_post_index_phone   varchar(255) not null

) engine INNODB;

create table books
(
    book_id             int          not null primary key auto_increment,
    book_name           varchar(255) not null unique,
    book_author_name    varchar(255) not null,
    book_published_year varchar(255) not null,
    book_price          varchar(255) not null,
    book_sale_count     int,
    book_insert_count   int,
    book_insert_date    varchar(255) not null

) engine INNODB;

create table book_orders
(
    order_id   int          not null primary key auto_increment,
    client     int          not null,
    book       int          not null,
    book_price varchar(255) not null,
    sale_date  varchar(255) not null,

    FOREIGN KEY (book) REFERENCES books (book_id)
        ON UPDATE CASCADE ON DELETE RESTRICT,

    FOREIGN KEY (client) REFERENCES client (client_id)
        ON UPDATE CASCADE ON DELETE RESTRICT
) engine INNODB;

